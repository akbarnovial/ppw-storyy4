from django import forms
from .models import Person
##Create your forms here
class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ('name', 'hobby', 'favorite_food','favorite_drink','year')

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control fontfriends'}),
            'hobby': forms.TextInput(attrs={'class': 'form-control'}),
            'favorite_food': forms.TextInput(attrs={'class': 'form-control'}),
            'favorite_drink': forms.TextInput(attrs={'class': 'form-control'}),
            'year': forms.Select(attrs={'class': 'form-control'}),
        }
