
from django.shortcuts import render,redirect
from .models import Person, ClassYear
from .forms import PersonForm
# Create your views here.
def home(request):
    return render(request,'pages/index.html',{})

def friends(request):
    if request.method == "POST":
        form = PersonForm(request.POST)
        if form.is_valid():
            form.save()
            
    else:
        form = PersonForm()

    persons = Person.objects.all()
    classyear = ClassYear.objects.all()

    response = {'persons' : persons, 'classyear': classyear,'form':PersonForm}
    return render(request, 'pages/friends.html', response)
