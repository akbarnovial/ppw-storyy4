from django.urls import path
from . import views
from django.contrib import admin


app_name = 'tugas'

urlpatterns = [
    path('',views.home,name='index'),
    path('friends/', views.friends, name='friends'),
    path('admin/', admin.site.urls),


]
