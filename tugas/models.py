from django.db import models

# Create your models here.
class ClassYear(models.Model):
    Batches = [('Omega  (2016)', 'Omega  (2016)'), ('Tarung   (2017)', 'Tarung   (2017)'), ('Quanta (2018)', 'Quanta (2018)'), ('Maung   (2019)', 'Maung   (2019)'), ('Others', 'Others')]
    batchyogs = models.CharField(max_length=30,choices=Batches, default = 'Quanta(2018)')

    def __str__(self):
        return self.batchyogs

class Person(models.Model):
    name = models.CharField(max_length=30, primary_key=True)
    hobby = models.CharField(max_length=30)
    favorite_food = models.CharField(max_length=30)
    favorite_drink = models.CharField(max_length=30)
    year = models.ForeignKey(ClassYear, on_delete = models.CASCADE, default= "")

    def __str__(self):
        return self.name
